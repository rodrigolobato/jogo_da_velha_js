// VARIÁVEIS

let x = document.querySelector(".x");
let o = document.querySelector(".o");
let boxes = document.querySelectorAll(".box");
let buttons = document.querySelectorAll("#buttons-container button");
let messageContainer = document.querySelector("#message");
let messageText = document.querySelector("#message p");
let secondPlay;




// CONTADOR DE JOGADAS

let player1 = 0;
let player2 = 0;

// ADICIONANDO O CLICK DE CADA JOGADA

for (let i = 0; i < boxes.length; i++) {


    boxes[i].addEventListener("click", function () {

        let init = checkInit(player1, player2);

        if (this.childNodes.length == 0) {

            let cloneElement = init.cloneNode(true);

            this.appendChild(cloneElement);

            // COMPUTAR JOGADAS

            if (player1 == player2) {
                player1++;

                if (secondPlay == 'btn-2') {

                    // FUNÇÃO JOGANDO CONTRA A MAQUINA

                    computerplay();

                    player2++;

                }

            }
            else {
                player2++;
            }

            // VERIFICAR VENCEDOR

            checkWinCondition();


        }


    });
}

// BUTÕES PARA INICIAR O JOGO!

for (let i = 0; i < buttons.length; i++) {

    buttons[i].addEventListener("click", function () {

        secondPlay = this.getAttribute("class");

        for (let j = 0; j < buttons.length; j++) {
            buttons[j].style.display = 'none';
        }

        setTimeout(function () {

            let container = document.querySelector("#containner");
            container.classList.remove("hide");
        }, 500)
    })
}



// VERIFICAR QUEM VAI JOGAR


function checkInit(player1, player2) {
    if (player1 == player2) {
        init = x;
    }
    else {
        init = o;
    }
    return init;

}




// ANUNCIAR VENCEDOR

function checkWinCondition() {

    b1 = document.getElementById("block1");
    b2 = document.getElementById("block2");
    b3 = document.getElementById("block3");
    b4 = document.getElementById("block4");
    b5 = document.getElementById("block5");
    b6 = document.getElementById("block6");
    b7 = document.getElementById("block7");
    b8 = document.getElementById("block8");
    b9 = document.getElementById("block9");


    // CONDIÇÕES PARA VENCER HORIZONTAL


    if (b1.childNodes.length > 0 && b2.childNodes.length > 0 && b3.childNodes.length > 0) {

        let b1child = b1.childNodes[0].className;
        let b2child = b2.childNodes[0].className;
        let b3child = b3.childNodes[0].className;


        if (b1child == "x" && b2child == "x" && b3child == "x") {
            declareWinner("x");
        }
        else if (b1child == "o" && b2child == "o" && b3child == "o") {
            declareWinner("o");
        }


    }

    if (b4.childNodes.length > 0 && b5.childNodes.length > 0 && b6.childNodes.length > 0) {

        let b4child = b4.childNodes[0].className;
        let b5child = b5.childNodes[0].className;
        let b6child = b6.childNodes[0].className;

        if (b4child == "x" && b5child == "x" && b6child == "x") {
            declareWinner("x");
        }
        else if (b4child == "o" && b5child == "o" && b6child == "o") {
            declareWinner("o");
        }

    }

    if (b7.childNodes.length > 0 && b8.childNodes.length > 0 && b9.childNodes.length > 0) {

        let b7child = b7.childNodes[0].className;
        let b8child = b8.childNodes[0].className;
        let b9child = b9.childNodes[0].className;

        if (b7child == "x" && b8child == "x" && b9child == "x") {
            declareWinner("x");
        }
        else if (b7child == "o" && b8child == "o" && b9child == "o") {
            declareWinner("o");
        }
    }


    // CONDIÇÕES PARA VENCER VERTICAL

    if (b1.childNodes.length > 0 && b4.childNodes.length > 0 && b7.childNodes.length > 0) {

        let b1child = b1.childNodes[0].className;
        let b4child = b4.childNodes[0].className;
        let b7child = b7.childNodes[0].className;


        if (b1child == "x" && b4child == "x" && b7child == "x") {
            declareWinner("x");
        }
        else if (b1child == "o" && b4child == "o" && b7child == "o") {
            declareWinner("o");
        }


    }

    if (b2.childNodes.length > 0 && b5.childNodes.length > 0 && b8.childNodes.length > 0) {

        let b2child = b2.childNodes[0].className;
        let b5child = b5.childNodes[0].className;
        let b8child = b8.childNodes[0].className;

        if (b2child == "x" && b5child == "x" && b8child == "x") {
            declareWinner("x");
        }
        else if (b2child == "o" && b5child == "o" && b8child == "o") {
            declareWinner("o");
        }

    }

    if (b3.childNodes.length > 0 && b6.childNodes.length > 0 && b9.childNodes.length > 0) {

        let b3child = b3.childNodes[0].className;
        let b6child = b6.childNodes[0].className;
        let b9child = b9.childNodes[0].className;

        if (b3child == "x" && b6child == "x" && b9child == "x") {
            declareWinner("x");
        }
        else if (b3child == "o" && b6child == "o" && b9child == "o") {
            declareWinner("o");
        }

    }


    // CONDIÇÕES PARA VENCER DIAGONAL

    if (b1.childNodes.length > 0 && b5.childNodes.length > 0 && b9.childNodes.length > 0) {

        let b1child = b1.childNodes[0].className;
        let b5child = b5.childNodes[0].className;
        let b9child = b9.childNodes[0].className;


        if (b1child == "x" && b5child == "x" && b9child == "x") {
            declareWinner("x");
        }
        else if (b1child == "o" && b5child == "o" && b9child == "o") {
            declareWinner("o");
        }


    }

    if (b3.childNodes.length > 0 && b5.childNodes.length > 0 && b7.childNodes.length > 0) {

        let b3child = b3.childNodes[0].className;
        let b5child = b5.childNodes[0].className;
        let b7child = b7.childNodes[0].className;

        if (b3child == "x" && b5child == "x" && b7child == "x") {
            declareWinner("x");
        }
        else if (b3child == "o" && b5child == "o" && b7child == "o") {
            declareWinner("o");
        }

    }


    // DEU VELHA - EMPATE, NENHUM JOGADOR GANHOU!

    let counter = 0;

    for (let i = 0; i < boxes.length; i++) {
        if (boxes[i].childNodes[0] != undefined) {
            counter++;
        }
    }

    if (counter == 9) {
        declareWinner("Deu Velha!");
    }

}

// RESET JOGO, E ODECLARA O VENCEDOR E ATUALIZA O PLACAR.

function declareWinner(winner) {

    let scoreboardX = document.querySelector("#scoreboard-1");
    let scoreboardO = document.querySelector("#scoreboard-2");
    let msg = '';


    if (winner == "x") {
        scoreboardX.textContent = parseInt(scoreboardX.textContent) + 1;
        msg = "O jogador 1 é o vencedor!";
      
    }
    else if (winner == "o") {
        scoreboardO.textContent = parseInt(scoreboardO.textContent) + 1;
        msg = "O jogador 2 é o vencedor!";
    }
    else {
        msg = "Deu Velha!"
        console.log("Deu Velha!");
    }



    // MOSTRAR VENCEDOR!

    messageText.innerHTML = msg;
    messageContainer.classList.remove("hide");


    // ESCONDER MENSSAGEM APÓS MOSTRADA

    setTimeout(function () {
        messageContainer.classList.add("hide");
    }, 800);


    // ZERAR JOGADAS E LIMPAR O JOGO!

    player1 = 0;
    player2 = 0;

    let boxesToRemove = document.querySelectorAll(".box div");

    for (let i = 0; i < boxesToRemove.length; i++) {
        boxesToRemove[i].parentNode.removeChild(boxesToRemove[i]);
    }


}


// JOGANDO CONTA A MAQUINA

function computerplay() {

    let cloneO = o.cloneNode(true);

    counter = 0;
    filled = 0;


    for (let i = 0; i < boxes.length; i++) {

        let randomNumber = Math.floor(Math.random() * 5);


        // SÓ PREENCHER SE ESTIVER VAZIO O FILHO

        if (boxes[i].childNodes[0] == undefined) {
            if (randomNumber <= 1) {
                boxes[i].appendChild(cloneO);
                counter++;
                break;
            }
        }

        // CHECAR QUANTAS ESTÃO PREENCHIDAS

        else {
            filled++;
        }
    }

    if (counter == 0 && filled < 9) {
        computerplay();
    }
}

